{
  "always_run_in_app" : true,
  "icon" : {
    "color" : "brown",
    "glyph" : "code"
  },
  "name" : "View JSON 1",
  "script" : "enum PDFThumbnailLayoutMode : Int8Array(size)",
  "share_sheet_inputs" : [
    "file-url",
    "url",
    "plain-text",
    "image"
  ]
}