// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// always-run-in-app: true; icon-color: brown;
// icon-glyph: code; share-sheet-inputs: file-url, url, plain-text, image;
using form validation

// JavaScript Document
$(function() {
  $('#wm-form').submit(function() {
    var errors = false;
    var errorMsg = "";
    $('.required').each(function() {
      if(!validField($(this))) {
        errorMsg += $(this).attr('name').capitalize() + " cannot be blank\n";
        errors = true;
      }
    });
    var emailAddress = $('#email');
    if(isValid(emailAddress) && !(/^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/.test(emailAddress.val()))) {
      errorMsg += "Not a valid email address. Please enter in a correctly formatted email address";
      errors = true;
    }
    if(errors) {
      alert(errorMsg);
      return false;
    }
  });

  $('.form-focus').click(function() {
    $(document).scrollTop(0);
    $('#first_name').focus();
    return false;
  });
});

function validField(element) {
  if(!isValid(element.val()) || (element.attr('placeholder') && element.attr('placeholder') == element.val()) || 
    (element.attr('type') == 'radio' && !checkedRadio(element))) {
    return false;
  }
  else {
    return true;
  }
}

function isValid(ele) {
  if(ele == null || ele == '') {
    return false;
  }
  else {
    return true;
  }
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

function checkedRadio (element) {
  var valid = false;
  $('input[name="'+ element.attr("name") +'"]:checked').each(function() {
    valid = true;
  });

  return valid;
}​